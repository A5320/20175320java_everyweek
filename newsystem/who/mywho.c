#include	<stdio.h>
#include    <stdlib.h>
#include	<utmp.h>
#include	<fcntl.h>
#include	<unistd.h>
#include <time.h>
#define	SHOWHOST	
void gettime(long timeval){
	char *p;
	p=ctime(&timeval);
	printf("%12.12s",p+4);
}
int show_info( struct utmp *utbufp )
{
	if(utbufp->ut_type!=USER_PROCESS){
		return 0;
	}
	printf("%-8.8s", utbufp->ut_name);	
	printf(" ");				
	printf("%-8.8s", utbufp->ut_line);	
	printf(" ");
	gettime(utbufp->ut_time);

	printf("%10ld", utbufp->ut_time);	
	printf(" ");				
#ifdef	SHOWHOST
	printf("(%s)", utbufp->ut_host);	
#endif
	printf("\n");				

	return 0;
}
int main()
{
	struct utmp	 current_record;	
	int		utmpfd;		
	int		reclen = sizeof(current_record);

	if ( (utmpfd = open("/var/run/utmp", O_RDONLY)) == -1 ){
		perror( UTMP_FILE );	
		exit(1);
	}
	while ( read(utmpfd, &current_record, reclen) == reclen )
		show_info(&current_record);
	close(utmpfd);
	return 0;			
}


