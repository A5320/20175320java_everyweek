import java.util.StringTokenizer;
import java.util.Stack;

public class MyDC {
    /**
     * constant for addition symbol
     */
    private final String ADD = "+";
    private final String SUBTRACT = "-";
    private final String MULTIPLY = "*";
    private final String DIVIDE = "/";
    private final String MODID = "%";
    private Stack<Integer> stack;


    public MyDC() {
        stack = new Stack<Integer>();
    }

    public int evaluate(String expr) {
        int op1, op2, result = 0, i = 0;
        String token[] = new String[3];
        StringTokenizer tokenizer = new StringTokenizer(expr);
        while (tokenizer.hasMoreTokens()) {
            token[i] = tokenizer.nextToken();
            if (i < 2) {
                i++;
            }
        }
        /* char token[] = expr.toCharArray();*/
        for (i = 0; i < 3; i++) {
            if (isOperator(token[i])) {
                op2 = (stack.pop()).intValue();
                op1 = (stack.pop()).intValue();
                result = evalSingleOp(token[i], op1, op2);
                stack.push(result);//将结果压栈
            } else
                stack.push(new Integer(Integer.parseInt(token[i])));
        }
        return result;
    }

    private boolean isOperator(String token) {
        return (token.equals("+") || token.equals("-") ||
                token.equals("*") || token.equals("/")||token.equals("%"));
    }

    private int evalSingleOp(String operation, int op1, int op2) {
        int result = 0;
        switch (operation) {
            case ADD:
                result = op1 + op2;
                break;
            case SUBTRACT:
                result = op1 - op2;
                break;
            case MULTIPLY:
                result = op1 * op2;
                break;
            case DIVIDE:
                result = op1 / op2;
                break;
            case MODID:
                result = op1 % op2;
        }
        return result;
    }

}
