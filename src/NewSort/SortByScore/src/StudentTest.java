import java.util.*;
/**
 * @author gong9
 */
public class StudentTest {
    public static void main(String[] args) {
        LinkedList<Student> list= new LinkedList<Student>();
        list.add(new Student("20175319","jy",'男', 19,95,93,96));
        list.add(new Student("20175318","lhr",'男', 19,91,93,92));
        list.add(new Student("20175320","gsj",'男', 19,96,97,92));
        list.add(new Student("20175321","wdc",'男', 19,92,93,92));
        list.add(new Student("20175322","zyh",'男', 19,93,95,91));
        Iterator<Student> iter=list.iterator();
        while(iter.hasNext()){
            Student te=iter.next();
            te.getTotalScore();
            System.out.print(te.name+" 总分为：");
            System.out.println(te.total_score);
        }
        Collections.sort(list);
        iter=list.iterator();
        System.out.println("按学号排序后的顺序为：");
        while(iter.hasNext()){
            Student stu=iter.next();
            System.out.println(stu.name);
        }


    }

}