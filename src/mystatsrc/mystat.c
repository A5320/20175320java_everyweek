#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
void main(int argc, char *argv[])
{
    struct stat sb;
    if (argc != 2) {
        fprintf(stderr, "Usage: %s <pathname>\n", argv[0]);
        exit(EXIT_FAILURE);
    }
    if (stat(argv[1], &sb) == -1) {
        perror("stat");
        exit(EXIT_FAILURE);
    }
    printf("File type:                ");
    switch (sb.st_mode & S_IFMT) {
        case S_IFBLK:   printf("block device\n");
            break;
    case S_IFCHR:   printf("character device\n");
            break;
    case S_IFDIR:   printf("directory\n");
            break;
    case S_IFIFO:   printf("FIFO/pipe\n");
            break;
    case S_IFLNK:   printf("symlink\n");
            break;
    case S_IFREG:   printf("regular file\n");
            break;
    case S_IFSOCK:  printf("socket\n");
            break;
    default:        printf("unknown?\n");
            break;
    }
    printf("Inode:            %ld\n", (long) sb.st_ino);
    printf("硬连接:               %ld\n", (long) sb.st_nlink);
    printf("UID=%ld   GID=%ld\n",(long) sb.st_uid, (long) sb.st_gid);
    printf("I/O块: %ld bytes\n",(long) sb.st_blksize);
    printf("大小:                %lld\n",(long long) sb.st_size);
    printf("块:         %lld\n",(long long) sb.st_blocks);
    printf("最近访问:         %s", ctime(&sb.st_atime));
    printf("最近更改:   %s", ctime(&sb.st_mtime));
    printf("最近改动:       %s", ctime(&sb.st_ctime));
 
    }

