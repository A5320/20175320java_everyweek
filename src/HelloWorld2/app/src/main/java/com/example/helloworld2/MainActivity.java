package com.example.helloworld2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);//覆盖了一个活动的生命周期方法，必须调用父类中被覆盖的方法
        setContentView(R.layout.activity_main);//使用R类调用清单中定义的样式，即layout中的activity.xml
    }
}
