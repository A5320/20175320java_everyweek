import java.io.*;
import java.util.*;
public class MyCP {
    public static void main(String[] args) {
        File file = new File(args[1]);
        File file2 = new File(args[2]);
        int a[]=new int[100];
       
        if(args[0].equals("-tx")){
            try{
            int i=1;
                String str=null;
                String temp=null;
                int b;
                Reader fis   = new FileReader(file);
                BufferedReader inData = new BufferedReader(fis);
                Writer fos = new FileWriter(file2);
                BufferedWriter outData = new BufferedWriter(fos);
                while(( str=inData.readLine())!=null){//从文件中读出数据
                    System.out.println(args[1]+"的第"+i+"个数字"+str);
                    b=Integer.parseInt((String)(str.trim()));//将字符串转化为int型数
                    temp=Integer.toBinaryString(b);//转化为二进制字符串
                    System.out.println(args[2]+"的第"+i+"个数字"+temp);
                    outData.write(temp);//写入目标文件
                    outData.newLine();
                }
                inData.close();
                fis.close();
                outData.close();
                fos.close();
            }
            catch(IOException e){ }
        }
        else if(args[0].equals("-xt")){
            try{
            int i=1;
                String str=null;
                String temp=null;
                int b;
                Reader fis   = new FileReader(file);
                BufferedReader inData = new BufferedReader(fis);
                Writer fos = new FileWriter(file2);
                BufferedWriter outData = new BufferedWriter(fos);
                while(( str=inData.readLine())!=null){
                    System.out.println(args[1]+"的第"+i+"个数字"+str);
                    b=Integer.parseInt(str.trim(),2);
                    temp=String.valueOf(b);
                    System.out.println(args[2]+"的第"+i+"个数字"+temp);
                    i++;
                    outData.write(temp);
                    outData.newLine();
                }
                inData.close();
                fis.close();
                outData.close();
                fos.close();
            }
            catch(IOException e){
            }
        }
    }
}
