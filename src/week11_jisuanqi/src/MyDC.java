import java.util.StringTokenizer;
import java.util.Stack;

public class MyDC {


    private final String ADD = "+";
    private final String SUBTRACT = "-";
    private final String MULTIPLY = "*";
    private final String DIVIDE = "/";
    private final String MODID = "%";
    private final String LPARE = "(";
    private final String RPARE = ")";
    private Stack<Integer> stack;
    private Stack<String> stacktwo;

    public MyDC() {
        stack = new Stack<Integer>();
        stacktwo = new Stack<String>();
    }

    public int evaluate(String expr) {
        int op1, op2, result = 0, i = 0;
        String token[] = new String[3];
        StringTokenizer tokenizer = new StringTokenizer(expr);
        while (tokenizer.hasMoreTokens()) {
            token[i] = tokenizer.nextToken();
            if (i < 2) {
                i++;
            }
        }
        /* char token[] = expr.toCharArray();*/
        for (i = 0; i < 3; i++) {
            if (isOperator(token[i])) {
                op2 = (stack.pop()).intValue();
                op1 = (stack.pop()).intValue();
                result = evalSingleOp(token[i], op1, op2);
                stack.push(result);//将结果压栈
            } else
                stack.push(new Integer(Integer.parseInt(token[i])));
        }
        return result;
    }

    private boolean isOperator(String token) {
        return (token.equals("+") || token.equals("-") ||
                token.equals("*") || token.equals("/")||token.equals("%"));
    }

    private int evalSingleOp(String operation, int op1, int op2) {
        int result = 0;
        switch (operation) {
            case ADD:
                result = op1 + op2;
                break;
            case SUBTRACT:
                result = op1 - op2;
                break;
            case MULTIPLY:
                result = op1 * op2;
                break;
            case DIVIDE:
                result = op1 / op2;
                break;
            case MODID:
                result = op1 % op2;
        }
        return result;
    }

    public String trans(String before) {
        int i = 0, j = 0;
        /*char step[] = before.toCharArray();
        char after[] = new char[step.length];*/
        String step[] = new String[3];
        StringTokenizer tokenizer = new StringTokenizer(before);
        while (tokenizer.hasMoreTokens()) {

            step[i] = tokenizer.nextToken();
            if(step[i].equals("×"))
            {
                step[i]=step[i].replaceAll("×","*");
            }
            if (i < 2) {
                i++;
            }
        }
        String after[] = new String[step.length];
        for (i = 0; i < 3; i++) {
            if (/*Integer.parseInt(step[i]) >= 0 && Integer.parseInt(step[i]) <= 9*/isNum(step[i])) {
                after[j] = step[i];
                j++;
                if (i == (step.length) - 1) {
                    while (stacktwo.empty() == false) {
                        after[j] = stacktwo.pop();
                        j++;
                    }
                }

            }
            else if(step[i].equals("%")){
                String str = step[0]+" "+ step[2]+" "+ step[1];
                return str;
            }
            else if (stacktwo.empty() || step[i].equals("(") || stacktwo.peek().equals("(") || stacktwo.peek().equals(")")) {
                stacktwo.push(step[i]);
                if (i == (step.length) - 1) {
                    while (stacktwo.empty() == false) {
                        j++;
                    }
                }
            } else if (step[i].equals(")")) {
                while (!stacktwo.peek().equals("(")) {
                    after[j] = stacktwo.pop();
                    j++;
                }
                stacktwo.pop();
                if (i == (3) - 1) {
                    while (stacktwo.empty() == false) {
                        after[j] = stacktwo.pop();
                        j++;
                    }
                }
            } else {
                switch (step[i]) {
                    case ADD:
                    case SUBTRACT: {
                        while (stacktwo.peek().equals("*") || stacktwo.peek().equals("/") || stacktwo.peek().equals("+") || stacktwo.peek().equals("-")) {
                            after[j] = stacktwo.pop();
                            j++;
                            if (stacktwo.empty()) {
                                break;
                            }
                        }
                        stacktwo.push(step[i]);
                        break;
                    }
                    case MULTIPLY:
                    case DIVIDE: {
                        while (stacktwo.peek().equals("*") || stacktwo.peek().equals("/")) {
                            after[j] = stacktwo.pop();
                            j++;
                            if (stacktwo.empty()) {
                                break;
                            }
                        }
                        stacktwo.push(step[i]);
                        break;
                    }
                }
                if (i == (3) - 1) {
                    while (stacktwo.empty() == false) {
                        after[j] = stacktwo.pop();
                        j++;
                    }
                }
            }
        }
        String str = after[0]+" "+ after[1]+" "+ after[2];
        return str;
    }

    public boolean isNum(String str) {
        int i;
        for (i = str.length()-1; i >= 0; i--) {
            int chr = str.charAt(i);
            if (chr < 48 || chr > 57)
                return false;
        }
        return true;
    }

}

