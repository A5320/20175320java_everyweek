import java.util.*;

public class Calc {
    public static void main(String[] args) {
        MyDC evaluator = new MyDC();
        String str;
        int result = 0;
        try {
            if (args.length != 3) {
                System.out.println("Usage: java Calc operato1 operand(+ - * / %) operator2");
            }
            //+ - x / 和%运算
            str = args[0]+" " + args[1] +" "+ args[2];
            result = evaluator.evaluate(evaluator.trans(str));
            System.out.println(args[0] + " " + args[1] + " " + args[2] + " = " + result);
        } catch (Exception IOException) {
            System.out.println("Input exception reported");
        }
    }
}

