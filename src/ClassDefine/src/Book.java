public class Book {
    String Name,Author,Press,Date;
    Book(){
        Name="Unknow";
        Author="Unknow";
        Press="Unknow";
        Date="Unknow";
    }
    Book(String a,String b,String c,String d) {
        Name=a;
        Author=b;
        Press=c;
        Date=d;
    }
    Book(String e){
        Name=e;
        Author=e;
        Press=e;
        Date=e;
    }
    public String getName() {
        return this.Name;
    }
    public String getAuthor() {
        return this.Author;
    }
    public String getPress() {
        return this.Press;
    }
    public String getDate() {
        return this.Date;
    }
    public void setMessage(String a,String b,String c,String d) {
        this.Name=a;
        this.Author=b;
        this.Press=c;
        this.Date=d;
    }
    public boolean equals(Object one) {
        Book two = (Book)one;
        if(one==null) return false;
        if(one==this) return true;
        if(!(one instanceof Book)) return false;
        if(two.Name==this.Name&&two.Author==this.Author&&two.Press==this.Press&&two.Date==this.Date) return true;
        else return false;
    }
    public String toString() {
        return ("书名:"+this.Name+'\n'+"作者:"+this.Author+'\n'+"出版社:"+this.Press+'\n'+"出版日期:"+this.Date+'\n');
    }
    public int hashCode() {
        int result=1;
        result=31*result+((Name==null)?0:Name.hashCode());
        result=31*result+((Author==null)?0:Author.hashCode());
        result=31*result+((Press==null)?0:Press.hashCode());
        result=31*result+((Date==null)?0:Date.hashCode());
        return result;
    }
}

