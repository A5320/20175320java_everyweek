import java.util.*;
public class Bookshelf {
    public static void main(String[] args) {
       Book numa,numb,numc;
        String a,b,c,d;
        int answer,i,dir;
        Scanner scanner = new Scanner(System.in);
        System.out.println("是否在构造时初始化，输入1为[yes]：");
        answer=scanner.nextInt();
        if(answer==1){
            System.out.println("输入三本书的信息");
            for(i=0;i<3;i++){
                System.out.print("输入书本序号：");
                dir=scanner.nextInt();
                switch(dir)
                {
                    case 1: {
                        scanner.nextLine();
                        a=scanner.nextLine();
                        b=scanner.nextLine();
                        c=scanner.nextLine();
                        d=scanner.nextLine();
                        numa=new Book(a,b,c,d);
                        System.out.println("第一本书修改后的信息为：");
                        System.out.println(numa.toString());
                        break;
                    }
                    case 2: {
                        scanner.nextLine();
                        a=scanner.nextLine();
                        b=scanner.nextLine();
                        c=scanner.nextLine();
                        d=scanner.nextLine();
                        numb=new Book(a,b,c,d);
                        System.out.println("第二本书修改后的信息为：");
                        System.out.println(numb.toString());
                        break;
                    }
                    case 3: {
                        scanner.nextLine();
                        a=scanner.nextLine();
                        b=scanner.nextLine();
                        c=scanner.nextLine();
                        d=scanner.nextLine();
                        numc=new Book(a,b,c,d);
                        System.out.println("第三本书修改后的信息为：");
                        System.out.println(numc.toString());
                        break;
                    }
                }
            }

        }
        else {
            numa=new Book();
            numb=new Book();
            numc=new Book();
            System.out.println("请输入修改后三本书的信息");
            for(i=0;i<3;i++){
                System.out.print("输入书本序号：");
                dir=scanner.nextInt();
                switch(dir)
                {
                    case 1: {
                        scanner.nextLine();
                        a=scanner.nextLine();
                        b=scanner.nextLine();
                        c=scanner.nextLine();
                        d=scanner.nextLine();
                        numa.setMessage(a,b,c,d);
                        break;
                    }
                    case 2: {
                        scanner.nextLine();
                        a=scanner.nextLine();
                        b=scanner.nextLine();
                        c=scanner.nextLine();
                        d=scanner.nextLine();
                        numb.setMessage(a,b,c,d);
                        break;
                    }
                    case 3: {
                        scanner.nextLine();
                        a=scanner.nextLine();
                        b=scanner.nextLine();
                        c=scanner.nextLine();
                        d=scanner.nextLine();
                        numc.setMessage(a,b,c,d);
                        break;
                    }
                }
            }
	    System.out.println();
	    System.out.println("修改后的书名为:");
            System.out.println(numa.toString());
            System.out.println(numb.toString());
            System.out.println(numc.toString());
	     if(numa.equals(numb)&&numb.equals(numc)&&numa.equals(numc))
                System.out.println("三本书相同");
            else
                System.out.println("三本书不完全相同");
        }
    }
}


