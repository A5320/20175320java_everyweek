import java.io.*;
import java.net.*;
import java.util.Scanner;
import java.util.*;

public class Client {
    public static void main(String args[]) {
        String expression;
        String result;
        Socket mysocket;
        DataInputStream in=null;
        DataOutputStream out=null;

        Scanner input = new Scanner(System.in);
        MyDC evaluator = new MyDC();
        System.out.println("input what you want:");
        expression = input.nextLine();
        result = evaluator.trans(expression);
        System.out.println("after trans:"+ result);

        try{  mysocket=new Socket("127.0.0.1",2010);
            in=new DataInputStream(mysocket.getInputStream());
            out=new DataOutputStream(mysocket.getOutputStream());
            out.writeUTF(result);
            String  s=in.readUTF();   //in readmessage
            System.out.println("Client get the answer:"+s);
            Thread.sleep(500);

        }
        catch(Exception e) {
            System.out.println("服务器已断开"+e);
        }
    }
}
