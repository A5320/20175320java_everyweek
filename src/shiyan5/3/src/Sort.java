public class Sort {
    char step[];
    char temp;
    String result;
    public String charSort(String daipai){//将原本无序的字符进行排序，使符号相同的字符串具有相同的顺序
        step= new char[daipai.length()];
        step=daipai.toCharArray();
        for(int i=0;i<daipai.length()-1;i++){
            for(int j=i+1;j<daipai.length();j++){
                if(step[i]>step[j]){
                    temp=step[i];
                    step[i]=step[j];
                    step[j]=temp;
                }
            }
        }
        result=new String(step);
        return result;
    }
}
