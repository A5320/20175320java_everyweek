import java.io.*;
import java.util.*;

public class MyDCTester {
    public static void main(String[] args) {

        String expression, step,before,wukuohao;
        StringTokenizer devide;
        Sort sort = new Sort();
        int a,b,c,d,rig=0, title, i, range,j,choose,k,find=0,get=0;
        String quchong[];
        String ceshi[];
        Rational result = new Rational();
        Rational now = new Rational();
        Language lang = new Language();
        Scanner in = new Scanner(System.in);
        File file = new File("title.txt");
        MyDC evaluator = new MyDC();
        Produce produce = new Produce();
        System.out.println("1：简体中文");
        System.out.println("2：繁體中文");
        System.out.println("3：English");
        choose=in.nextInt(); //选择语言
        System.out.println(lang.lang(choose,true,0));//提示输入题目数目
        title = in.nextInt();
        System.out.println(lang.lang(choose,true,0));//输入数据范围
        range = in.nextInt();
        in.nextLine(); //读取输入的回车

         quchong = new String[title];
         ceshi = new String[title];

        String answer[] = new String[title];
        try {//缓冲输入流需使用try-catch语句
            for (i = 0; i < title; i++) {
                do {//检验生成的计算题是否合法（是否需要计算复数）
                    before = produce.output(range);
                    expression=before.replaceAll("÷","/");//将生成的"÷"号转化为"/"号
                    wukuohao=expression.replaceAll("[[(])]","");//去除生成算式的括号
                    devide=new StringTokenizer(wukuohao);
                    quchong[i] = devide.nextToken();
                    while(devide.hasMoreTokens()==true){
                        quchong[i]=devide.nextToken()+quchong[i];//将去除括号后的算式记录在字符串数组中
                    }
                    quchong[i]=sort.charSort(quchong[i]);//对去除括号后的算式内的字符进行排序，使得具有相同字符个体的字符串具有相同的形式
                    if(i>=1){
                        for(k=i-1;k>=0;k--){
                            if (quchong[i]==quchong[k]) {
                                find=1;//记录是否有相同的字符串，若相同，find=1。
                            }
                        }
                    }
                    now = evaluator.evaluate(evaluator.trans(expression));
                    c = now.getNumerator();
                    d = now.getDenominator();
                    if (d == 1) {//计算生成题目的结果，作为去重的参考
                        ceshi[i] = String.valueOf(c);
                    } else if (c == 0) {
                        ceshi[i] = String.valueOf(0);
                    } else if (c > d) {
                        ceshi[i] = (c / d + "\'" + (c % d) + "/" + d);
                    } else {
                        ceshi[i] = (c + "/" + d);
                    }
                    if(i>=1){
                        for(k=i-1;k>=0;k--){
                            if (ceshi[i]==ceshi[k]) {
                                get=1;//若生成的算式与前面已生成算式的计算结果相同，get=1。
                            }
                        }
                    }

                } while (c < 0||(find==1&&get==1));//判断生成的题目是否合法并去重
                if (i == 0) {//若生成的是第一题，则刷新文件
                    Writer out = new FileWriter("title.txt");
                    BufferedWriter bufferWrite = new BufferedWriter(out);
                    bufferWrite.write(before);
                    bufferWrite.newLine();
                    bufferWrite.close();
                    out.close();
                } else {//不是第一题则从文件后开始输入
                    Writer out = new FileWriter("title.txt", true);//将生成的题目输入到文件中
                    BufferedWriter bufferWrite = new BufferedWriter(out);
                    bufferWrite.write(before);
                    bufferWrite.newLine();
                    bufferWrite.close();
                    out.close();
                }
                System.out.println(lang.lang(choose,true,title) + (i + 1) + "：" + before);//将题目输出
                System.out.println(lang.lang(choose,false,title));//根据所选语言输出“你的回答”
                answer[i] = in.nextLine();//记录用户输入的回答


            }
            Reader input = new FileReader("title.txt");
            BufferedReader bufferRead = new BufferedReader(input);
            System.out.println();
            System.out.println(lang.lang(choose,true,0));
            for(j=0;j<title;j++){
                result = evaluator.evaluate(evaluator.trans((bufferRead.readLine()).replaceAll("÷","/")));//从文件中获取题目，并将题目中的÷"号转化为"/"号
                a = result.getNumerator();
                b = result.getDenominator();
                if (b == 1) {//计算生成题目的结果，并将结果转化为真分数
                    step = String.valueOf(a);
                } else if (a == 0) {
                    step = String.valueOf(0);
                } else if (a > b) {
                    step = (a / b + "\'" + (a % b) + "/" + b);
                } else {
                    step = (a + "/" + b);
                }
                if(step.equals(answer[j])) {//判断是否正确，使用变量rig记录正确的题数
                    System.out.println((j+1)+lang.lang(choose,step.equals(answer[j]),title));
                    rig++;
                }
                else{
                    System.out.println((j+1)+lang.lang(choose,step.equals(answer[j]),title));
                }

            }
            bufferRead.close();
            input.close();
            System.out.println(lang.lang(choose,true,0)+((float)rig/title)*100+"%");//输出正确率

        } catch (IOException e) {
            System.out.println(e.toString());
        }
    }

}

