public class Language {
    String ste;
    int i=0,j=0;//通过i和j控制返回的字符串在主方法中的位置
    public String lang(int m,boolean cha,int time) {//变量m选择语言，变量cha控制同一循环中输出的字符串，变量time记录主方法中的循环次数
        if (i == 0) {
            if (m == 1) {
                 ste=("输入题目数目");
            } else if (m == 2) {
                ste= ("輸入題目數目");
            } else {
                ste= ("Number of input questions");
            }
            i++;
        } else if (i == 1) {
            if (m == 1) {
                ste= ("输入数据范围");
            } else if (m == 2) {
                ste= ("輸入數據範圍");
            } else {
                ste= ("Input data range");
            }
            i++;
        } else if (i == 2) {
            if(j<(time*2)) {//循环输出字符串"题目"与"你的回答"时控制字符串输出的位置
                if (cha == true) {//控制输出同一循环中是输出"题目"还是输出"你的回答"
                    if (m == 1) {
                        ste = ("题目");
                    } else if (m == 2) {
                        ste = ("題目");
                    } else {
                        ste = ("The title");
                    }
                } else {
                    if (m == 1) {
                        ste = ("你的回答是：");
                    } else if (m == 2) {
                        ste = ("妳的回答是：");
                    } else {
                        ste = ("Your answer is yes:");
                    }
                }
                j++;
            }
            if(j==(time*2)) {//结束循环则进入下一个需要不同语言的位置
                i++;
            }
        }
        else if (i == 3) {
            if (m == 1) {
                ste= ("结果：");
            } else if (m == 2) {
                ste= ("結果：");
            } else {
                ste= ("result");
            }
            j=0;
            i++;
        }
        else if (i == 4) {
            if(j<time) {
                if (cha == true) {
                    if (m == 1) {
                        ste = ("题正确");
                    } else if (m == 2) {
                        ste = ("題正確");
                    } else {
                        ste = ("right");
                    }
                } else {
                    if (m == 1) {
                        ste = ("题错误");
                    } else if (m == 2) {
                        ste = ("題錯誤");
                    } else {
                        ste = ("wrong");
                    }
                }
                j++;
            }
            if(j==time) {
                i++;
            }
        } else {
            if (m == 1) {
                ste= ("正确率为：");
            } else if (m == 2) {
                ste= ("正確率為：");
            } else {
                ste= ("correct:");
            }
            i=0;
        }
        return ste;
    }

    }