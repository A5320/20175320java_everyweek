import java.util.*;
public class Produce {
    int fuhao;//随机生成算式的符号总数
    int kuohao; //随机生成的括号组数
    int jiajian; //括号内随机生成的加减乘除
    int size; //括号外随机生成的加减乘除
    int shuzi; //随机生成数字
    String m,n,p,q;//存放数字
    String a,b,c;//存放符号
    char suanshi [] = new char[12];
    Random random = new Random();//使用Random类生成随机数
    public String output(int range) {//变量range控制随机生成的数字在0到rang-1之间
        String out;
        fuhao=random.nextInt(3)+1;//随机生成运算符号和括号的总数，运算符号数最多三个，括号最多两组，根据这两个随机数结合实际情况随机生成题目
        kuohao=random.nextInt(3);
        m=suijishu(range);
        n=suijishu(range);
        p=suijishu(range);
        q=suijishu(range);
        a=suijihao();
        b=suijihao();
        c=suijihao();
        if(fuhao==1) {//判断随机生成的符号总数
            out= (m+a+n);
        }
        else if(fuhao==2){
            if(kuohao==0){//判断随机生成的括号组数
                    out= (m+a+n+b+p);
                }
            else
                {
                    out= ("("+m+a+n+")"+b+p);
                }
            }

        else {
            if(kuohao==0){

                   out= (m+a+n+b+p+c+q);
                }
            else if(kuohao==1)
                {
                    out= ("("+m+a+n+b+p+")"+c+q);
                }
            else
                {
                    out= ("("+m+a+n+")"+b+"("+p+c+q+")");
                }
            }
        suanshi=out.toCharArray();
        return out;
    }
    public String suijishu(int m){//生成随机数
        shuzi=random.nextInt(m);
        if(shuzi==0){
            return this.score(m);
        }
        else{

            return String.valueOf(shuzi);
        }
    }
    public String suijihao() {//随机生成运算符号
        size=random.nextInt(4);
        String result;
        switch (size){
            case 0:
               result="+";
                break;
            case 1:
                result="-";
                break;
            case 2:
                result="*";
                break;
            default:
               result="÷";
        }
        return result;
    }
    public String score(int n) {//随机生成分数
        int a, b, temp;
        Random rand = new Random();
        do {
            a = rand.nextInt(n);
            b = rand.nextInt(n);
        } while (a == 0 || b == 0);
        if (a > b) {
            temp = a;
            a = b;
            b = temp;
        }
        return (a +"/"+ b);
    }
}
