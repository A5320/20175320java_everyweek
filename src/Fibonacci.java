import java.util.Scanner;
class  Function {
    public int fib(int m) {
        if (m == 1 || m == 2) {
            return 1;
        } else {
            return fib(m - 1) + fib(m - 2);
        }
    }
}
public class Fibonacci{
    public static void main(String[] args) {
        Function object;
       int m,i,fibonacci[];
       object = new Function();
       Scanner scanner = new Scanner(System.in);
       System.out.println("输入斐波那契数的个数：\n");
       m=scanner.nextInt();
       while(m<=0){
           System.out.println("error,input again.\n");
           m=scanner.nextInt();
           if(m>0){
               break;
           }
       }
        fibonacci = new int[m];
       for(i=0;i<m;i++){
           fibonacci[i]=object.fib(i+1);
       }
       for(i=0;i<m;i++){
           System.out.printf("第%d个数是 %d\n",i+1,fibonacci[i]);
       }

    }
}

