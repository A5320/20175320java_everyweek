public class MyList {
    public static void main(String [] args) {
        //选用合适的构造方法，用你学号前后各两名同学的学号创建四个结点
        //把上面四个节点连成一个没有头结点的单链表
        Node<Integer> insert = new Node(5320,null);
        Node<Integer> d = new Node(5322,null);
        Node<Integer> c = new Node(5321,d);
        Node<Integer> b = new Node(5319,c);
        Node<Integer> a = new Node(5318,b);
        Node<Integer> temp = a;
        Node<Integer> tempThis=null;
        System.out.println("遍历单链表，打印每个结点的数据");
        MyList.outPut(a);
        System.out.println("插入新数据");
        temp=a;
        tempThis=a;
        while(temp.next!=null){
            if(temp.data>5320){
                tempThis.next=insert;
                insert.next=temp;
            }
            tempThis=temp;
            temp=temp.next;
        }
        System.out.println("遍历单链表，打印每个结点的数据");
        MyList.outPut(a);
        System.out.println("删除部分数据");
        temp=a;
        tempThis=a;
        while(temp.next!=null){

            if(temp.data==5320){
                tempThis.next=temp.next;
            }
            tempThis=temp;
            temp=temp.next;
        }
        System.out.println("遍历单链表，打印每个结点的数据");
        MyList.outPut(a);
    }
    public static void outPut(Node<Integer> temp){
        while(temp.next!=null){
            System.out.println(temp.data);
            temp=temp.next;
        }
        System.out.println(temp.data);
    }
}
