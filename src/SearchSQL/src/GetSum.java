import java.sql.*;
public class GetSum {
    public static void main(String[] args) {
        Connection con;
        Statement sql;
        ResultSet rs;
        String min,max;

        con = GetDBConnection.connectDB("world","root","");
        if(con==null){
            return;
        }
        String sqlStr="select * from country where Region='Middle East'";
        try{
            sql=con.createStatement();
            rs=sql.executeQuery(sqlStr);
            int population=0;
            while(rs.next()){
                population=population+rs.getInt(7);
            }
            System.out.printf("中东的总人口为：%d\n",population);
            con.close();
        }
        catch(SQLException e){
            System.out.println(e);
        }
    }
}
