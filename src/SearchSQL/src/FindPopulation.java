import java.sql.*;
/**
 * @author gong9
 */
public class FindPopulation {
    public static void main(String[] args) {
        Connection con;
        Statement sql;
        ResultSet rs;
        String min,max;

        con = GetDBConnection.connectDB("world","root","");
        if(con==null){
            return;
        }
        String sqlStr="select * from country order by LifeExpectancy";
        try{
            sql=con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
            rs=sql.executeQuery(sqlStr);
            rs.first();
            min=rs.getString(2);
            rs.last();
            max=rs.getString(2);
            System.out.printf("最短为：%s\n",min);
            System.out.printf("最长为：%s\n",max);
            con.close();
        }
        catch(SQLException e){
            System.out.println(e);
        }
    }

}
