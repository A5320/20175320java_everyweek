class CPU {
	int speed;
	void setSpeed(int m) {
		this.speed = m;
	}
	int getSpeed() {
		return this.speed;
	}
}
class HardDisk {
	int amount;
	void setAmount(int m) {
		this.amount = m;
	}
	int getAmount() {
		return this.amount;
	}
}
class PC {
	CPU cpu;
	HardDisk HD;
	void setCPU(CPU c) {
		cpu = c;
	}
	void setHardDisk(HardDisk h) {
		HD = h;
	}
	void show() {
		System.out.println(cpu.speed);
		System.out.println(HD.amount);
	}
}
public class Test {
	public static void main(String args[]) {
		CPU cpu = new CPU();
		HardDisk disk = new HardDisk();
		PC pc = new PC();
		cpu.speed = 2200;
		disk.amount = 200;
		pc.setCPU(CPU cpu);
		pc.setHardDisk(HardDisk disk);
		pc.show();
	}
}


