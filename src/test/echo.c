/*
 * echo - read and echo text lines until client closes connection
 */
/* $begin echo */
#include "csapp.h"

void echo(int connfd,char *haddrp) 
{
    time_t t; 
    struct tm * lt;
    
    size_t n; 
    char buf[MAXLINE]; 
    rio_t rio;

    Rio_readinitb(&rio, connfd);
    while((n = Rio_readlineb(&rio, buf, MAXLINE)) != 0) { //line:netp:echo:eof
    printf("\n客户端IP：%s\n",haddrp);
    printf("服务器实现者学号：20175320\n");
    time (&t);
    lt = gmtime (&t);
    printf ("当前时间为：%d/%d/%d %d:%d:%d\n",lt->tm_year+1900, lt->tm_mon+1, lt->tm_mday, lt->tm_hour, lt->tm_min, lt->tm_sec);

    
	/*printf("server received %d bytes\n", (int)n);*/
	Rio_writen(connfd, buf, 6);
    }
}
/* $end echo */

