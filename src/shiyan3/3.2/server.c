#include<netinet/in.h>   
#include<sys/types.h>   
#include<sys/socket.h>   
#include<stdio.h>   
#include<stdlib.h>   
#include<string.h>   
#include<pthread.h>  
#define HELLO_WORLD_SERVER_PORT    175317  
#define LENGTH_OF_LISTEN_QUEUE     20  
#define BUFFER_SIZE                1024  
#define FILE_NAME_MAX_SIZE         512  
void *process_client(void *new_server_socket);
int mywc(char file_name[])
{
    char ch;
    int flag=0,num=0;
    int choose;
    FILE *fp;
    printf("统计单词个数还是实现“wc -w”?(1or2)\n");
    scanf("%d",&choose);
    if((fp = fopen(file_name,"r"))==NULL)
    {
        printf("Failure to open %s\n",file_name);
        exit(0);
    }

    if(choose==1)
    {
    while((ch=fgetc(fp))!=EOF)
    {
        if(ch==' ' || ch=='\n' || ch=='\t' ||  ch=='\!' || ch=='\?' || ch=='\"' || ch=='\.' || ch== '\,' || ch=='\:' || ch=='\(' || ch=='\)' || ch=='\;' || ch=='\-')
        {
            flag=0;
        }
        else
        {
            if(flag==0)
            {
                flag=1;
                num++;
            }

        }
                   
    }

    }
    else if(choose==2)
    {
        while((ch=fgetc(fp))!=EOF)
        {
            if(ch==' ' || ch=='\n' || ch=='\t' || ch=='\r')
                flag=0;
            else
            {
                if(flag==0)
                {
                    flag=1;
                    num++;
                }
            }
        }
    }
    printf("单词个数为：%d\n",num);
    fclose(fp);
    return num;
}
int main(int argc, char **argv)  
{  
// set socket's address information   
// 设置一个socket地址结构server_addr,代表服务器internet的地址和端口  
    struct sockaddr_in   server_addr;  
    bzero(&server_addr, sizeof(server_addr));  
    server_addr.sin_family = AF_INET;  
    server_addr.sin_addr.s_addr = htons(INADDR_ANY);  
    server_addr.sin_port = htons(HELLO_WORLD_SERVER_PORT);  
// create a stream socket   
// 创建用于internet的流协议(TCP)socket，用server_socket代表服务器向客户端提供服务的接口  
    int server_socket = socket(PF_INET, SOCK_STREAM, 0);  
    if (server_socket < 0)  
    {  
        printf("Create Socket Failed!\n");  
        exit(1);  
    }  

// 把socket和socket地址结构绑定   
    if (bind(server_socket, (struct sockaddr*)&server_addr, sizeof(server_addr)))  
    {  
        printf("Server Bind Port: %d Failed!\n", HELLO_WORLD_SERVER_PORT);  
        exit(1);  
    }  

// server_socket用于监听   
    if (listen(server_socket, LENGTH_OF_LISTEN_QUEUE))  
    {  
        printf("Server Listen Failed!\n");  
        exit(1);  
    }  
// 服务器端一直运行用以持续为客户端提供服务   
      
    while(1)
    {
// 定义客户端的socket地址结构client_addr，当收到来自客户端的请求后，调用accept  
// 接受此请求，同时将client端的地址和端口等信息写入client_addr中  
        struct sockaddr_in client_addr;  
        socklen_t length = sizeof(client_addr);  


        int new_server_socket = accept(server_socket, (struct sockaddr*)&client_addr, &length);  
        printf("连接到客户端\n");
        if (new_server_socket < 0)  
        {  
            printf("Server Accept Failed!\n");  
              
        }
        //添加进程相关代码
        pthread_t pid;
        if(pthread_create(&pid, NULL, process_client,(void *) &new_server_socket) < 0){
              printf("pthread_create error\n");
        }
        
    }
//  close(server_socket);
}
void *process_client(void *new_server_socket)
{
        int sockid=*(int *)new_server_socket;
        FILE *fp;
        //接受来自客户端的文件
        char buffer[BUFFER_SIZE]; 
        char file_name[FILE_NAME_MAX_SIZE];
        bzero(buffer, sizeof(buffer));  
        int length=0;
        if(recv(sockid,buffer,BUFFER_SIZE, 0)==-1)
        {
            printf("接受文件名%s失败\n",buffer);
        }
        strcpy(file_name,buffer);
        strcat(file_name,"-server");
        if((fp = fopen(file_name,"w"))==NULL)
        {
            printf("Failure to open %s\n",file_name);
            exit(0);
        }
        while( length = recv(sockid, buffer, BUFFER_SIZE, 0))
        {
            if(length<0)
            {
                printf("接受文件出错\n");
                exit(0);
            }
            
            if(fwrite(buffer,sizeof(char),length,fp)<length)
            {
                printf("写文件失败\n");
            }
            bzero(buffer, BUFFER_SIZE);
        }
        fclose(fp);
        printf("接受文件完毕\n");
        int number=0;
        number=mywc(file_name);
        bzero(buffer, BUFFER_SIZE);  
        buffer[0]=number+48;
 
     
        bzero(buffer, sizeof(buffer));  
        

        printf("File Transfer Finished!\n");    
        close(new_server_socket);  
} 
