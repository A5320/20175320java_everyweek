#include<netinet/in.h>                         // for sockaddr_in  
#include<sys/types.h>                          // for socket  
#include<sys/socket.h>                         // for socket  
#include<stdio.h>                              // for printf  
#include<stdlib.h>                             // for exit  
#include<string.h>                             // for bzero  
  
#define HELLO_WORLD_SERVER_PORT       165330  
#define BUFFER_SIZE                   1024  
#define FILE_NAME_MAX_SIZE            512  
int mywc(char file_name[],int choose);
int main(int argc, char **argv)  
{  
    FILE *fp;
    if (argc != 2)  
    {  
        printf("Usage: ./%s ServerIPAddress\n", argv[0]);  
        exit(1);  
    }  

// 设置一个socket地址结构client_addr, 代表客户机的internet地址和端口  
    struct sockaddr_in client_addr;  
    bzero(&client_addr, sizeof(client_addr));  
    client_addr.sin_family = AF_INET; // internet协议族  
    client_addr.sin_addr.s_addr = htons(INADDR_ANY); // INADDR_ANY表示自动获取本机地址  
    client_addr.sin_port = htons(0); // auto allocated, 让系统自动分配一个空闲端口  

// 创建用于internet的流协议(TCP)类型socket，用client_socket代表客户端socket  
    int client_socket = socket(AF_INET, SOCK_STREAM, 0);  
    if (client_socket < 0)  
    {  
        printf("Create Socket Failed!\n");  
        exit(1);  
    }  

// 把客户端的socket和客户端的socket地址结构绑定   
    if (bind(client_socket, (struct sockaddr*)&client_addr, sizeof(client_addr)))  
    {  
        printf("Client Bind Port Failed!\n");  
        exit(1);  
    }  

// 设置一个socket地址结构server_addr,代表服务器的internet地址和端口  
    struct sockaddr_in  server_addr;  
    bzero(&server_addr, sizeof(server_addr));  
    server_addr.sin_family = AF_INET;  

// 服务器的IP地址来自程序的参数   
    if (inet_aton(argv[1], &server_addr.sin_addr) == 0)  
    {  
        printf("Server IP Address Error!\n");  
        exit(1);  
    }                                                                                                               
    server_addr.sin_port = htons(HELLO_WORLD_SERVER_PORT);  
    socklen_t server_addr_length = sizeof(server_addr);  
    // 向服务器发起连接请求，连接成功后client_socket代表客户端和服务器端的一个socket连接  
    if (connect(client_socket, (struct sockaddr*)&server_addr, server_addr_length) < 0)  
    {  
        printf("Can Not Connect To %s!\n", argv[1]);  
        exit(1);  
    }  

    char file_name[FILE_NAME_MAX_SIZE + 1];  
    bzero(file_name, sizeof(file_name));  
    printf("Please Input File Name.\t");  
    scanf("%s", file_name);  
    if((fp = fopen(file_name,"r"))==NULL)
    {
         printf("Failure to open %s\n",file_name);
         exit(0);
    }
    
    char buffer[BUFFER_SIZE];  
    bzero(buffer, sizeof(buffer));  
    strcpy(buffer,file_name);
    if(send(client_socket,buffer,BUFFER_SIZE,0)==-1)
    {
        printf("发送文件名失败\n");
    }
    char ch;
    int i=0;
    while((ch=fgetc(fp))!=EOF)
    {
        buffer[i++]=ch;
        if(i>=BUFFER_SIZE)
        {
            if((send(client_socket, buffer, BUFFER_SIZE, 0))==-1)
            {
                printf("发送文件失败\n");
            }
            bzero(buffer, sizeof(buffer));
            i=0;
        }
    }
    if(i<BUFFER_SIZE)
    {
        if((send(client_socket, buffer, i, 0))==-1)
        {
            printf("发送文件失败\n");
        }
    }
    printf("发送%s完毕\n",file_name);
    mywc(file_name,1);
     mywc(file_name,2);
    // 向服务器发送buffer中的数据，此时buffer中存放的是客户端需要接收的文件 
    //以下接收服务器发来的单词个数
    bzero(buffer, sizeof(buffer));

    fclose(fp);  
    close(client_socket);  
    return 0;  

}  
 int mywc(char file_name[],int choose)
  {
       FILE *fp;
       char ch;
        int flag=0,num=0;
       
            if((fp = fopen(file_name,"r"))==NULL)
             {
                  printf("Failure to open %s\n",file_name);
                   exit(0);
                }
             
                  if(choose==1)
                   {
                    while((ch=fgetc(fp))!=EOF)
                     {
                          if(ch==' ' || ch=='\n' || ch=='\t' ||  ch=='\!' || ch=='\?' || ch=='\"' || ch=='\.' || ch== '\,' || ch=='\:' || ch=='\(' || ch=='\)' || ch=='\;'     || ch=='\-')
                          {
                          flag=0;
                          }
                          else
                          {
                          if(flag==0)
                          {
                          flag=1;
                          num++;
                          }
                          
                          }

                    }

                }
                else if(choose==2)
                {
                    while((ch=fgetc(fp))!=EOF)
                    {
                        if(ch==' ' || ch=='\n' || ch=='\t' || ch=='\r')
                        flag=0;
                        else
                        {
                            if(flag==0)
                            {
                                flag=1;
                                num++;
                            }
                        }
                    }
                }
                printf("单词个数为：%d_用方式%d计算\n",num,choose);
                fclose(fp);
                return num;
}
